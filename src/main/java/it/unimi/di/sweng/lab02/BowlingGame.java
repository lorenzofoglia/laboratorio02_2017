package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	private int punteggio = 0;
	private int[] tab = new int[20];
	private int i = 0;

	@Override
	public void roll(int pins) {
		
		tab[i]=pins;
		i++;
	}

	@Override
	public int score() {
		for(int i =0;i<20;i++){
			if(i%2==0 && i!=0 )
				if(tab[i-1]+tab[i-2]==10)
						tab[i]*=2;
		punteggio += tab[i];
	}
	return punteggio;
	}

}
